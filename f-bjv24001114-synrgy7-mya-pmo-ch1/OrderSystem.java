package challenge.challenge1;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class OrderSystem {
    private static Map<String, Double> menu = new HashMap<>();
    private static Map<String, Integer> orders = new HashMap<>();
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        initializeMenu();
        runOrderSystem();
    }

    private static void runOrderSystem() {
        boolean isRunning = true;
        while (isRunning) {
            displayMenu();
            int choice = getUserChoice();
            switch (choice) {
                case 1:
                    placeOrder();
                    break;
                case 2:
                    confirmAndPay();
                    break;
                case 3:
                    isRunning = false;
                    break;
                default:
                    System.out.println("Pilihan tidak valid. Silakan pilih kembali.");
                    break;
            }
        }
    }

    private static int getUserChoice() {
        return scanner.nextInt();
    }

    //    menampilkan menu pilihan kepada pengguna
    private static void displayMenu() {
        System.out.println("\n===============================");
        System.out.println("Selamat Datang Di Restoran Kami");
        System.out.println("===============================");
        System.out.println("1. Pesan Makanan");
        System.out.println("2. Konfirmasi dan Pembayaran");
        System.out.println("3. Keluar");
        System.out.print("Pilihan Anda: ");
    }

    //    memasukan menu dan harga ke daftar menu
    private static void initializeMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1. Mie Goreng", 12000.0);
        menu.put("2. Nasi Goreng", 15000.0);
        menu.put("3. Ayam Goreng", 18000.0);
        menu.put("4. Soto Ayam", 20000.0);
    }

    //    memilih menu makanan
    private static void placeOrder() {
        displayFoodMenu();
        int menuNumber = getUserInput("Masukkan nomor makanan yang ingin dipesan: ");
        String selectedFood = findSelectedFood(menuNumber);
        if (selectedFood != null) {
            int quantity = getUserInput("Masukkan jumlah pesanan: ");
            updateOrder(selectedFood, quantity);
        } else {
            System.out.println("Nomor makanan tidak valid.");
        }
    }

    //    menampilkan daftar menu
    private static void displayFoodMenu() {
        System.out.println("\n======= Menu Makanan =======");
        for (String item : menu.keySet()) {
            System.out.println(item + " - Rp " + menu.get(item));
        }
        System.out.println("============================");
    }

    //    memilih menu
    private static int getUserInput(String message) {
        System.out.print(message);
        return scanner.nextInt();
    }

    //    mencari menu dari input
    private static String findSelectedFood(int menuNumber) {
        int index = 1;
        for (String food : menu.keySet()) {
            if (index == menuNumber) {
                return food;
            }
            index++;
        }
        return null;
    }

    //    memperbarui jumlah makanan yang dipilih
    private static void updateOrder(String selectedFood, int quantity) {
        if (orders.containsKey(selectedFood)) {
            quantity += orders.get(selectedFood);
        }
        orders.put(selectedFood, quantity);
        System.out.println("Pesanan " + selectedFood + " sebanyak " + quantity + " berhasil ditambahkan.");
    }

    //    konfimasi pembayaran
    private static void confirmAndPay() {
        double total = calculateTotal();
        displayOrderDetails(total);
        boolean isConfirmed = getConfirmation();
        if (isConfirmed) {
            saveReceipt(total);
            clearOrders();
        } else {
            System.out.println("Pembayaran dibatalkan.");
        }
    }

    //    menghitung total harga
    private static double calculateTotal() {
        double total = 0;
        for (String item : orders.keySet()) {
            int quantity = orders.get(item);
            double price = menu.get(item);
            total += quantity * price;
        }
        return total;
    }

    //    detail dari tagihan
    private static void displayOrderDetails(double total) {
        System.out.println("\n===== Konfirmasi dan Pembayaran =====");
        for (String item : orders.keySet()) {
            int quantity = orders.get(item);
            double price = menu.get(item);
            double subtotal = quantity * price;
            System.out.println(item + " - " + quantity + " x Rp " + price + " = Rp " + subtotal);
        }
        System.out.println("---------------------------------------+");
        System.out.println("Total Pembayaran: Rp " + total);
    }

    //    konfirmasi untuk membayar
    private static boolean getConfirmation() {
        System.out.print("Apakah Anda yakin ingin melakukan pembayaran? (y/n): ");
        String confirmation = scanner.next();
        return confirmation.equalsIgnoreCase("y");
    }

    //    mencetak struk
    private static void saveReceipt(double total) {
        try {
            FileWriter writer = new FileWriter("struk_pembayaran.txt");
            writer.write("===== Struk Pembayaran =====\n");
            for (String item : orders.keySet()) {
                int quantity = orders.get(item);
                double price = menu.get(item);
                double subtotal = quantity * price;
                writer.write(item + " - " + quantity + " x Rp " + price + " = Rp " + subtotal + "\n");
            }
            writer.write("---------------------------------------+");
            writer.write("\nTotal Pembayaran: Rp " + total);
            writer.close();
            System.out.println("Struk pembayaran telah disimpan sebagai file struk_pembayaran.txt");
        } catch (IOException e) {
            System.out.println("Terjadi kesalahan dalam menyimpan struk pembayaran.");
        }
    }

    //    menghapus semua pesanan yang sudah di order
    private static void clearOrders() {
        orders.clear();
    }
}